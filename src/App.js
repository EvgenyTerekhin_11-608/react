import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

    constructor() {
        super();
        this.state = {
            users: [
                {
                    name: "Evgeny", age: "20"
                },
                {
                    name: "Evgeny", age: "20"
                },
                {
                    name: "Evgeny", age: "20"
                }
            ]
        }
    }

    addUser() {
        let users = [...this.state.users, {name: 'Evgeny', age: 20}];
        this.setState({
            users: users
        });
    }

    deleteUser() {
        let users = this.state.users;
        let length = users.length;
        users = users.splice(1, length - 1);
        this.setState({users: users});
    }

    render() {
        const {users} = this.state;
        const userItems = users.map(x => <li>{x.name}</li>);
        return (
            <div className="App">
                <button onClick={this.addUser.bind(this)}>Добавить пользователя</button>
                <button onClick={this.deleteUser.bind(this)}>Удалить пользователя</button>
                <ol>{userItems}</ol>
            </div>
        );
    }
}

export default App;
